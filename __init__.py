# hi, i am here
# this should make things work
# yeah


class Protein:
    def __init__(self, tart):
        self.tart = tart

        tagok = self.tart.split(" ")
        os1 = []
        for i in range (len(tagok)):
            if tagok[i].startswith("OS="):
                tagok[i] = tagok[i].split("=")[1] + " " + tagok[i+1]
                os1.append(tagok[i])
            if tagok[i].startswith("OX="):
                break
        self.os = " ".join(os1)

        tagok = self.tart.split(" ")
        name1 = []
        for i in tagok:
            if i.startswith("|"):
                i = i.split("|")[2]
            if i.startswith("OS=") == False:
                name1.append(i)
            if i.startswith("OS="):
                break
        self.name = " ".join(name1)

        tagok = self.tart.split(" ")
        for i in range (len(tagok)):
            if tagok[i].startswith("OX="):
                self.ox = int(tagok[i][3: ])

        tagok = self.tart.split(" ")
        for i in range (len(tagok)):
            if tagok[i].startswith("GN="):
                self.gn = tagok[i][3: ]

        tagok = self.tart.split(" ")
        for i in range (len(tagok)):
            if tagok[i].startswith("PE="):
                self.pe = int(tagok[i][3: ])

        tagok = self.tart.split(" ")
        for i in range (len(tagok)):
            if tagok[i].startswith("SV="):
                sv1 = tagok[i].split("\n")
                self.sv = int(sv1[0][3: ])


        tagok = self.tart.split(" ")
        x = ""
        for i in range (len(tagok)):
            if tagok[i].startswith("SV="):
                size1 = tagok[i].split("\n")
                size1 = size1[1: -1]
                for j in range (len(size1)):
                    x = x + size1[j]
        self.size = len(x)


        tagok = self.tart.split(" ")
        name3 = []
        for i in tagok:
            if i.startswith("|"):
                i = i.split("|")[1]
                name3.append(i)
                break
        self.id = name3[0]


        tagok = self.tart.split(" ")
        x = ""
        for i in range (len(tagok)):
            if tagok[i].startswith("SV="):
                size1 = tagok[i].split("\n")
                size1 = size1[1: -1]
                for j in range (len(size1)):
                    x = x + size1[j]
        self.aa = x
        aminoacidcounts = {
            "G" : self.aa.count("G"),
            "Y" : self.aa.count("Y"),
            "W" : self.aa.count("W"),
            "A" : self.aa.count("A"),
            "R" : self.aa.count("R"),
            "N" : self.aa.count("N"),
            "D" : self.aa.count("D"),
            "C" : self.aa.count("C"),
            "Q" : self.aa.count("Q"),
            "E" : self.aa.count("E"),
            "H" : self.aa.count("H"),
            "I" : self.aa.count("I"),
            "L" : self.aa.count("L"),
            "K" : self.aa.count("K"),
            "M" : self.aa.count("M"),
            "F" : self.aa.count("F"),
            "P" : self.aa.count("P"),
            "S" : self.aa.count("S"),
            "T" : self.aa.count("T"),
            "V" : self.aa.count("V")
        }

        self.aminoacidcounts = aminoacidcounts


    def kiirat(self):
        return self.tart

    def sort_proteins_pe_hez(self):
        tagok = self.tart.split(" ")
        for i in range (len(tagok)):
            if tagok[i].startswith("PE="):
                self.pe = int(tagok[i][3: ])
        return self.pe


    def __eq__(self, other):
        return ((self.size) == (other.size))

    def __ne__(self, other):
        return ((self.size) != (other.size))

    def __lt__(self, other):
        return ((self.size) < (other.size))

    def __le__(self, other):
        return ((self.size) <= (other.size))

    def __gt__(self, other):
        return ((self.size) > (other.size))

    def __ge__(self, other):
        return ((self.size) >= (other.size))



    def __repr__(self):
        return self.name + " id: " + self.id



def sort_proteins_pe(lista):
    lista4 = sorted(lista, key = Protein.sort_proteins_pe_hez, reverse = True)
    return lista4

def sort_proteins_aa(lista, a):
    lista3 = sorted(lista, key = lambda x: x.aa.count(a), reverse = True)
    return lista3


def find_protein_with_motif(lista, motif):
    nyami = []
    for i in range(len(lista)):
        if lista[i].aa.find(motif) > -1:
            nyami.append(lista[i])
    return nyami


def load_fasta(fasta):
    data = open(fasta,"r").read()
    data = data.split(">sp")
    names = []
    for i in data:
        if i.startswith("|") == True:
            names.append(i)
    proteins = []
    for i in range(len(names)):
        j = Protein(names[i])
        proteins.append(j)

    return proteins




